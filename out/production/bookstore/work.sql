CREATE TABLE Table1 (
    author_id bigserial primary key not null ,
    author varchar(255)
);
CREATE TABLE Table2 (
                        id bigserial primary key not null ,
                        author_id integer references Table1 (author_id),
                        book varchar(255),
                        count_sells int
);
insert into Table1 (author) values
('Пушкин А.С.'), ('Лермонтов М.Ю.'), ('Белинский В.Г.'), ('Крылов И.А.'), ('Тютчев Ф.И.');
insert into Table2 (book, count_sells) values
('Медный всадник', 5), ('Мцыри', 10), ('Люблю грозу в начале мая...Стихотворения, письма', 6), ('Басни Крылова', 15), ('Литературные мечтания', 8), ('Капитанская дочка', 14), ('Евгений Онегин', 22), ('Герой нашего времени', 13);
select Table2.book from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Пушкин А.С.';
select Table2.book from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Лермонтов М.Ю.';
select Table2.book from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Белинский В.Г.';
select Table2.book from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Крылов И.А.';
select Table2.book from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Тютчев Ф.И.';

select Table2.book, Table2.count_sells from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Пушкин А.С.';
select Table2.book, Table2.count_sells from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Лермонтов М.Ю.';
select Table2.book, Table2.count_sells from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Белинский В.Г.';
select Table2.book, Table2.count_sells from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Крылов И.А.';
select Table2.book, Table2.count_sells from Table2 inner join Table1 T on Table2.author_id = T.author_id where T.author = 'Тютчев Ф.И.';